export enum StorageType {
    User = "user_storage",
    ChatRoomConnection = "chat_room_connection_storage"
}

export class StorageService {

    public clearStorage(type?: StorageType): void {
        type ? localStorage.removeItem(type) : localStorage.clear();
    }

    public getStorageItem<t>(type: StorageType): t {
        const item = localStorage.getItem(type);
        return JSON.parse(item);
    }

    public writeStorageItem(type: StorageType, value: any): void {
        localStorage.setItem(type, JSON.stringify(value));
    }

}

export const storageService = new StorageService();
