import {SocketActions, webSocketService} from "./Services/WebSocket.service";
import {chromeService, MessageTypes} from "./Services/Chrome.service";
import {ChromeMessageModel} from "./Models/ChromeMessage.model";
import {ChatMessageModel} from "./Models/ChatMessage.model";

console.log("I LIVE");

let isReconnecting = false;

const connectToBackend = () => {
    webSocketService.connect().then(() => {
        chromeService.onMessage(MessageTypes.ConnectMessage, () => {
            const msg = new ChromeMessageModel(MessageTypes.ConnectMessage, webSocketService.isConnected);
            chromeService.sendMessage(msg);
        });

        changeRoomRouter();
        chatMessageRouter();

        if(intervalFunction) clearInterval(intervalFunction);

    }).catch((err) => {
        console.error(err, "happening in on webSocket.connect()");
        if(!isReconnecting) retryToConnect();
    });
};

let intervalFunction = null;
const retryToConnect = () => {
    isReconnecting = true;
    intervalFunction = setInterval(() => {
        console.log("trying to reconnect");
        connectToBackend();
    }, 1000);
};

const chatMessageRouter = () => {
    chromeService.onMessage<ChatMessageModel>(MessageTypes.ChatMessage, (message => {
        webSocketService.send({
            action: SocketActions.SendMessage,
            message: {
                author: message.sender.userName,
                imgUrl: message.sender.imageUrl,
                text: message.message
            }
        })
    }));

    webSocketService.onMessage(SocketActions.SendMessage, (data => {
        console.log(data, "this should go to the client layer");
    }));
};

const changeRoomRouter = () => {
    chromeService.onMessage<string>(MessageTypes.ChangeRoomMessage, (url) => {
        webSocketService.send({
            action: SocketActions.ChangeRoom,
            chatUrl: url
        });
    });

    webSocketService.onMessage(SocketActions.ChangeRoom, (dat) => {
        console.log("TODO save connection to room to storage", dat);
    });
};

connectToBackend();
