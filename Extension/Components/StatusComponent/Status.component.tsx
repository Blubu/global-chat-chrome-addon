import * as React from 'react';
import "./Status.style.scss"
import {StatusStore} from "./Status.store";
import {observer} from "mobx-react";
import {BiUser, BiCog} from 'react-icons/bi';
import * as ReactModal from "react-modal";
import {SetUserModal} from "./Modal/SetUser.modal";
import {UserModel} from "../../Models/User.model";

interface StatusComponentProps {
    store: StatusStore;
}

interface StatusComponentStates {
    isModal: boolean;
}

@observer
export class StatusComponent extends React.Component<StatusComponentProps, StatusComponentStates> {
    private connectingMessage = ["Connection lost", "Connecting...", "Connected"];
    private connectingClasses = ["off", "connecting", "on"];

    constructor(props: StatusComponentProps) {
        super(props);
        this.state = {
            isModal: false
        };
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div className={"status-component"}>
                <div className={"left"}>
                    {this.renderAvatar()}
                </div>
                <div className={"right"}>
                    <div title={this.connectingMessage[this.props.store.isConnected]} className={"connection-box " + this.connectingClasses[this.props.store.isConnected]}/>
                    <button className={"icon-button"}>
                        <BiCog className={"icon"}/>
                    </button>
                </div>
                <ReactModal isOpen={this.state.isModal} onRequestClose={() => {this.setState({isModal: false})}}>
                    <SetUserModal userModel={this.props.store.userModel} onAcceptModal={this.onAcceptModal.bind(this)}/>
                </ReactModal>
            </div>
        )
    }

    renderAvatar(): any {
        if(!this.props.store.userModel) {
            return <div className={"avatar"} onClick={this.onOpenModal.bind(this)}>
                <BiUser style={{width: "20px", height: "20px"}}/>
            </div>
        }
        if(this.props.store.userModel.isValidImage) {
            return <div className={"avatar avatar-image"} onClick={this.onOpenModal.bind(this)} style={{
                background: "url("+ this.props.store.userModel.imageUrl+")",
            }}/>
        } else {
            return <div className={"avatar"} onClick={this.onOpenModal.bind(this)}>
                <BiUser style={{width: "20px", height: "20px"}}/>
            </div>
        }
    }

    onOpenModal(): void {
        this.setState({isModal: true});
    }

    onAcceptModal(user: UserModel): void {
        this.setState({isModal: false});
        this.props.store.writeUserToStore(user);
    }
}
