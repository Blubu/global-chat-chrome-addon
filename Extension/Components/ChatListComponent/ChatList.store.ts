import {observable} from "mobx";
import Tab = chrome.tabs.Tab;

export class ChatListStore {

    @observable tabs: Tab[] = [];

    constructor() {}

    public getTabs(): void {
        chrome.tabs.query({currentWindow: true, active: true}).then(value => {
            this.tabs = value;
        });
    }

}
