import * as React from 'react';
import * as ReactDom from 'react-dom';
import "./styles.scss";
import {Layout} from "./layout";

ReactDom.render(<Layout/>, document.getElementById("root"));
