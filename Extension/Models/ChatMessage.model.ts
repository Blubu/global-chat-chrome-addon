import {UserModel} from "./User.model";

export class ChatMessageModel {
    constructor(
        public sender?: UserModel,
        public message?: string
    ) {}
}
