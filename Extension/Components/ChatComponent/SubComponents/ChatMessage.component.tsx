import * as React from 'react';
import "./ChatMessage.style.scss";
import {UserModel} from "../../../Models/User.model";

export interface ChatMessageComponentProps {
    sender: UserModel;
    message: string;
    isOther: boolean;
}
export interface ChatMessageComponentStates {}

export class ChatMessageComponent extends React.Component<ChatMessageComponentProps, ChatMessageComponentStates>{

    render() {
        return (
            <div className={"chat-message"}>
                <div className={"sender"}>{this.props.sender}</div>
                <div className={"message"}>{this.props.message}</div>
            </div>
        )
    }

}
