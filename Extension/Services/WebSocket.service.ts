import {CONNECTION_CONFIG} from "../Config/Connection.config";

export enum SocketActions {
    SendMessage = 'sendmessage',
    ChangeRoom = 'changeroom'
}

export class WebSocketService {
    public socket: WebSocket;
    public isConnected: boolean;
    private listeners: Map<SocketActions, ((data: any) => void)[]>;

    constructor() {
        this.listeners = new Map<SocketActions, ((data: any) => void)[]>();
    }

    public connect(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                this.socket = new WebSocket(CONNECTION_CONFIG.aws);
                this.socket.onopen = ev => {
                    this.isConnected = true;
                    resolve(true);
                };
                this.socket.onerror = ev => {
                    console.error(ev);
                    reject(ev);
                };
                this.socket.onmessage = (dat) => {
                    this.receiveMessageFromSocket(dat);
                };
                this.socket.onclose = ev => {
                    this.isConnected = false;
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    public disconnect(): void {
        this.socket.close(200);
    }

    public send<t>(msg: t): void {
        const str = JSON.stringify(msg);
        this.socket.send(str);
    }

    private receiveMessageFromSocket(data: any): void {
        console.log(data, "in websocket service received");
        const obj = JSON.parse(data);
        console.log(obj, "and parsed version");
        this.listeners.forEach((value, key) => {
            if(this.listeners.has(obj.data.action)) {
                for(const cb of value) {
                    cb(obj.data);
                }
            }
        });
    }

    public onMessage<t>(type: SocketActions, cb: (data: any) => void): void {
        if(this.listeners.has(type)) {
            const cbArray = this.listeners.get(type);
            cbArray.push(cb);
            this.listeners.set(type, cbArray);
        } else {
            this.listeners.set(type, [cb]);
        }
    }
}

export const webSocketService = new WebSocketService();
