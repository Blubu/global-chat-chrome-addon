import {RoomModel} from "../Models/Room.model";
import {Socket} from "socket.io";

export class RoomManager {

    public rooms: Map<string, RoomModel>;

    constructor() {
        this.rooms = new Map<string, RoomModel>();
    }

    public addSocketToRoom(socket: Socket, url: string): void {
        socket["activeRoom"] = url;
        socket.join(url);
        if(this.rooms.has(url)) {
            this.rooms.get(url).connections.push(socket);
            console.log("added to existing room");
        } else {
            const newRoom = new RoomModel();
            newRoom.connections.push(socket);
            this.rooms.set(url, newRoom);
            console.log("added new room: ", url, this.rooms.size);
        }
    }

    public removeSocketFromRoom(socket: Socket): void {
        const url = socket['activeRoom'];
        if(this.rooms.has(url)) {
            const room = this.rooms.get(url);
            let i = room.connections.indexOf(socket);
            room.connections.splice(i, 1);
            console.log("user removed from room: ", url, room.connections.length);
            if(room.connections.length == 0) {
                this.rooms.delete(url);
                console.log("room deleted because empty");
            }
        }
    }

}

export const roomManager = new RoomManager();
