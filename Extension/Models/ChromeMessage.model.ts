import {MessageTypes} from "../Services/Chrome.service";

export class ChromeMessageModel<t> {
    constructor(public name: MessageTypes, public payload: t) {}
}
