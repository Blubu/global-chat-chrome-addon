import * as React from 'react';
import "./SetUser.style.scss";
import {UserModel} from "../../../Models/User.model";

export interface SetUserModalProps {
    onAcceptModal: (user: UserModel) => void;
    userModel?: UserModel;
}
export interface SetUserModalStates {
    userName: string;
    userImage: string;
}

export class SetUserModal extends React.Component<SetUserModalProps, SetUserModalStates> {

    constructor(props: SetUserModalProps) {
        super(props);
        this.state = {
            userImage: this.props.userModel.imageUrl,
            userName: this.props.userModel.userName
        }
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div className={"set-user-modal"}>
                <div className={"input-row"}>
                    <div className={"left"}>
                        <span>Username:</span>
                        <span>ImageUrl:</span>
                    </div>
                    <div className={"right"}>
                        <input value={this.state.userName} onChange={(e) => {this.setState({userName: e.target.value})}}/>
                        <input value={this.state.userImage} onChange={(e) => {this.setState({userImage: e.target.value})}}/>
                    </div>
                </div>
                <div className={"image-preview"}>
                    <img src={this.state.userImage}/>
                </div>
                <div className={"actions-row"}>
                    <button onClick={this.acceptModal.bind(this)}>Accept</button>
                </div>

            </div>
        )
    }

    acceptModal(): void {
        const image = new Image();
        image.onload = () => {
            this.props.onAcceptModal(new UserModel(this.state.userName, this.state.userImage, true));
        };
        image.onerror = () => {
            this.props.onAcceptModal(new UserModel(this.state.userName, this.state.userImage, false));
        };
        image.src = this.state.userImage;
    }

}
