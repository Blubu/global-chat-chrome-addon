to start out do the following:
1. npm run copy
2. npm run build-client
3. go to your chrome browser under extensions
4. click "unpackaged extensions loading" (something like this) and navigate to the dist folder of the project
5. profit

if you want to run the backend
1. npm run build-server
2. npm run server
