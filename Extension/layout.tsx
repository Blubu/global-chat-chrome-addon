import * as React from "react";
import {StatusComponent} from "./Components/StatusComponent/Status.component";
import {ChatComponent} from "./Components/ChatComponent/Chat.component";
import {ChatStore} from "./Components/ChatComponent/Chat.store";
import {observer} from "mobx-react";
import {chromeService} from "./Services/Chrome.service";
import {StatusStore} from "./Components/StatusComponent/Status.store";
import {storageService} from "./Services/Storage.service";
import {ChatListComponent} from "./Components/ChatListComponent/ChatList.component";
import {ChatListStore} from "./Components/ChatListComponent/ChatList.store";

@observer
export class Layout extends React.Component<any, any> {

    public render() {
        return (
            <React.Fragment>
                <StatusComponent store={new StatusStore(chromeService, storageService)}/>
                <ChatComponent store={new ChatStore(storageService, chromeService)}/>
                {/*<ChatListComponent store={new ChatListStore()}/>*/}
            </React.Fragment>
        )
    }

}
