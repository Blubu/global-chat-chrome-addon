import {observable} from "mobx";
import {ChatMessageModel} from "../../Models/ChatMessage.model";
import {UserModel} from "../../Models/User.model";
import {StorageService, StorageType} from "../../Services/Storage.service";
import {ChromeService, MessageTypes} from "../../Services/Chrome.service";
import {ChromeMessageModel} from "../../Models/ChromeMessage.model";

export class ChatStore {
    @observable public messages: ChatMessageModel[] = [];
    @observable public user: UserModel;

    constructor(
        private storageService: StorageService,
        private chromeService: ChromeService
    ) {
        this.getUser();
    }

    public sendMessage(message: string): void {
        const msg = new ChromeMessageModel(MessageTypes.ChatMessage, new ChatMessageModel(
            this.user,
            message
        ));
        this.chromeService.sendMessage(msg);
    }

    private listenOnMessages(data: any): void {
        // let message = new ChatMessageModel(data.sender, data.message);
        // console.log("got new message", message);
        // this.messages.push(message);
    }

    private getUser(): void {
        this.user = this.storageService.getStorageItem(StorageType.User);
    }

}
