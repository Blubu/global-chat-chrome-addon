import {Socket} from "socket.io";

export class RoomModel {
    public id: string;
    public connections: Socket[] = [];
    public messages: Message[] = [];
}

class Message {
    public sender: string;
    public message: string;
}
