import * as express from 'express';
import * as http from 'http';
import {SOCKET_EVENTS} from "../Shared/SocketEvents";
import {roomManager} from "./Manager/Room.manager";

const app = express();
const server = http.createServer(app);

app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

const io = require('socket.io')(server);

io.on('connection', (socket)=> {
    console.log("a user is connected");
    socket.on(SOCKET_EVENTS.sendUrl, (data) => {
        roomManager.addSocketToRoom(socket, data);
    });

    socket.on('disconnect', () => {
        console.log("DISCONNECT");
        roomManager.removeSocketFromRoom(socket);
    });

    socket.on(SOCKET_EVENTS.sendChat, (data) => {
        console.log("received message: ", data, socket['activeRoom']);
        const url = socket['activeRoom'];
        io.in(url).emit(SOCKET_EVENTS.sendChat, data);
    });
});

const serverInstance = server.listen(80, ()=> {
    console.log("listening on port 80");
});
