import {ChromeService, MessageTypes} from "../../Services/Chrome.service";
import {observable} from "mobx";
import {ChromeMessageModel} from "../../Models/ChromeMessage.model";
import {StorageService, StorageType} from "../../Services/Storage.service";
import {UserModel} from "../../Models/User.model";

export class StatusStore {

    @observable public isConnected = 1;
    @observable public userModel: UserModel;

    constructor(
        private chromeService: ChromeService,
        private storageService: StorageService
    ) {
        this.checkIfConnected();
        this.fetchUserFromStorage();
    }

    public writeUserToStore(userModel: UserModel): void {
        this.storageService.writeStorageItem(StorageType.User, userModel);
        this.userModel = userModel;
    }

    public fetchUserFromStorage(): void {
        const model = this.storageService.getStorageItem<UserModel>(StorageType.User);
        if(model) {
            this.userModel = model;
        } else {
            this.userModel = new UserModel("Anon", "", false);
        }
    }

    private checkIfConnected(): void {
        this.chromeService.onMessage<boolean>(MessageTypes.ConnectMessage, value => {
            this.isConnected = value ? 2 : 0;
        });
        const chromeMessage = new ChromeMessageModel(MessageTypes.ConnectMessage, null);
        this.chromeService.sendMessage(chromeMessage);
    }

}
