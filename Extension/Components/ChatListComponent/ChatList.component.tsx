import * as React from 'react';
import "./ChatList.style.scss";
import {ChatListStore} from "./ChatList.store";
import Tab = chrome.tabs.Tab;
import {observer} from "mobx-react";

interface ChatListProps {
    store: ChatListStore;
}

interface ChatListStates {}

@observer
export class ChatListComponent extends React.Component<ChatListProps, ChatListStates> {

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div className={"chat-list-component"}>
                {this.renderTabs()}
            </div>
        )
    }

    renderTabs(): any {
        let elems = [];
        for(let i = 0; i < this.props.store.tabs.length; i++) {
            const tab: Tab = this.props.store.tabs[i];
            elems.push(<div className={"chat-list-entry"}>
                <img src={tab.favIconUrl} />
                <span>{tab.title}</span>
            </div>)
        }

        return elems;
    }

    componentDidMount(): void {
        this.props.store.getTabs();
    }

}
