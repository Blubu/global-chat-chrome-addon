import {ChromeMessageModel} from "../Models/ChromeMessage.model";

export enum MessageTypes {
    ChatMessage = 'chatMessage',
    ConnectMessage = 'connectMessage',
    ChangeRoomMessage = 'changeRoomMessage',
    DisconnectMessage = 'disconnectMessage'
}

export class ChromeService {

    private listeners: Map<MessageTypes, Function[]> = new Map<MessageTypes, Function[]>();

    constructor() {
        chrome.runtime.onMessage.addListener((message: ChromeMessageModel<any>) => {
            if(this.listeners.has(message.name)) {
                for(const listener of this.listeners.get(message.name)) {
                    listener(message.payload);
                }
            }
        });
    }

    public onMessage<t>(type: MessageTypes, cb: (data: t) => void): void {
        if(this.listeners.has(type)) {
            let tmp = this.listeners.get(type);
            tmp.push(cb);
            this.listeners.set(type, tmp);
        } else {
            this.listeners.set(type, [cb]);
        }
    }

    public sendMessage(msg: ChromeMessageModel<any>): void {
        chrome.runtime.sendMessage(msg);
    }
}

export const chromeService = new ChromeService();
