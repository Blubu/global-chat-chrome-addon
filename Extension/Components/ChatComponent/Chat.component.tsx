import * as React from 'react';
import "./Chat.style.scss";
import {ChatStore} from "./Chat.store";
import {observer} from "mobx-react";
import {ChatMessageComponent} from "./SubComponents/ChatMessage.component";
import {BiSend} from 'react-icons/bi';
import {BiSmile} from 'react-icons/bi';
import TextareaAutosize from 'react-textarea-autosize';

export interface ChatComponentProps {
    store: ChatStore;
}

export interface ChatComponentStates {
    message: string;
}

@observer
export class ChatComponent extends React.Component<ChatComponentProps, ChatComponentStates> {
    private keyCodes = {};

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
    }

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <div className={"chat-component"}>
                <div className={"content"}>
                    {this.renderMessages()}
                </div>
                <div className={"text"}>
                    <TextareaAutosize minRows={1} maxRows={3} onKeyDown={this.onKeyDown.bind(this)} onKeyUp={this.onKeyUp.bind(this)} value={this.state.message} onChange={(e)=> {
                        this.setState({message: e.target.value});
                    }}/>
                    <button className={"icon-button"}>
                        <BiSmile className={"icon"}/>
                    </button>
                    <button className={"icon-button"} onClick={this.sendMessage.bind(this)}>
                        <BiSend className={"icon"}/>
                    </button>
                </div>
            </div>
        )
    }

    private onKeyDown(e): void {
        this.keyCodes[e.keyCode] = true;
        if(this.keyCodes[13] && !this.keyCodes[17] && this.state.message.length) {
            e.preventDefault();
            this.sendMessage();
        }

        if(this.keyCodes[13] && this.keyCodes[17]) {
            this.setState({
                message: this.state.message + "\n"
            });
        }
    }

    private onKeyUp(e): void {
       this.keyCodes[e.keyCode] = false;
    }

    private sendMessage(): void {
        if(this.state.message.trim().length > 0) {
            this.props.store.sendMessage(this.state.message);
            this.setState({message: ''})
        }
    }

    private renderMessages(): any {
        let elems = [];
        for(let i = 0; i < this.props.store.messages.length; i++) {
            const message = this.props.store.messages[i];
            elems.push(<ChatMessageComponent sender={message.sender} message={message.message} isOther={false}/>)
        }
        return elems;
    }
}
